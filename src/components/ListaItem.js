import React, { Component } from "react";

class ListaItem extends Component {

  constructor(props) {
    super(props);
    this.listarItem = this.listarItem.bind(this);
  }
  delete(key) {
    this.props.delete(key);
  }

  

listarItem(item) {
    return <li onClick={() => this.delete(item.key)} key={item.key} >{item.text} - {item.type}</li>
  }
  
  
 
  render() {
    const itensAdicionados = this.props.entries;
    const listItems = itensAdicionados.map(this.listarItem);
 
    return (
      <ul className="lista">
          {listItems}
      </ul>
    );
  }

};

export default ListaItem;