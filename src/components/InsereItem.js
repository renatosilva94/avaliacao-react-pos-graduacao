import React, { Component } from "react";
import ListaItem from './ListaItem'
import Select from 'react-select';
var rand = require("random-key");

class InsereItem extends Component {

  constructor(props) {
    super(props);
    this.adicionarItem = this.adicionarItem.bind(this);
    this.deletarItem = this.deletarItem.bind(this);
    this.state = {
      itens: []
    };
  }

  adicionarItem(e) {
    if (this._inputElement.value !== "") {
      const addItem = {
        text: this._inputElement.value,
        type: this._selectElement.value,
        key: rand.generate()
      };

      this.setState((prevState) => {
        return {
          itens: prevState.itens.concat(addItem)
        };
      });

      this._inputElement.value = "";
    }

    e.preventDefault();
  }

  deletarItem(key) {
    const delItem = this.state.itens.filter(function (item) {
      return (item.key !== key);
    });
    this.setState({
      itens: delItem
    });
  }

  onChange = (event) => {
    this.setState({ value: event.target.value });
  };

  render() {
    return (
      <div className="header">
        <form onSubmit={this.adicionarItem}>

          <input ref={(a) => this._inputElement = a}
            placeholder="Nome do Item" />

          <select ref={(a) => this._selectElement = a} value={this.state.value} onChange={this.onChange}>
            <option value="Fruta">Fruta</option>
            <option value="Banho">Banho</option>
            <option value="Cozinha">Cozinha</option>
            <option value="Sala">Sala</option>


          </select>

          <button type="submit">+</button>
        </form>

        <ListaItem entries={this.state.itens} delete={this.deletarItem} />

      </div>
    );
  }
}

export default InsereItem;